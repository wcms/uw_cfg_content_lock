<?php

/**
 * @file
 * uw_cfg_content_lock.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_content_lock_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer checked out documents'.
  $permissions['administer checked out documents'] = array(
    'name' => 'administer checked out documents',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'content_lock',
  );

  // Exported permission: 'check out documents'.
  $permissions['check out documents'] = array(
    'name' => 'check out documents',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'content_lock',
  );

  return $permissions;
}
